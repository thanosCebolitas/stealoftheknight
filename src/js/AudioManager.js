function AudioManager(urlList, callback) {
  this.context = new window.AudioContext();
  this.urlList = urlList;
  this.onload = callback;
  this.bufferTable = {};
  this.loadCount = 0;
}

AudioManager.prototype.loadBuffer = function(url, index) {
  // Load buffer asynchronously
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.responseType = "arraybuffer";

  var loader = this;

  request.onload = function() {
    // Asynchronously decode the audio file data in request.response
    loader.context.decodeAudioData(
      request.response,
      function(buffer) {
        if (!buffer) {
          alert('error decoding file data: ' + url);
          return;
        }
        loader.bufferTable[index] = {
          "buffer": buffer,
          "isPlaying": false
        };
        if (++loader.loadCount == Object.values(loader.urlList).length && loader.onload)
          loader.onload(loader.bufferTable);
      },
      function(error) {
        console.error('decodeAudioData error', error);
      }
    );
  }

  request.onerror = function() {
    alert('AudioManager: XHR error');
  }

  request.send();
}

AudioManager.prototype.load = function() {
  for (var key in this.urlList) {
    this.loadBuffer(this.urlList[key], key);
  }
}

AudioManager.prototype.playSound = function(soundIndex, loop, force) {
  if (!this.bufferTable[soundIndex].isPlaying || force) {
    var source = this.context.createBufferSource();
    source.buffer = this.bufferTable[soundIndex].buffer;
    this.bufferTable[soundIndex].isPlaying = true;
    // source.detune.value = -1200;
    // source.playbackRate.value = 0.5;
    source.connect(this.context.destination);
    source.loop = loop;
    var that = this;
    source.onended = function () {
      that.bufferTable[soundIndex].isPlaying = false;
    };
    source.start(0);
  }
}
