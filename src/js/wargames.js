//------------------------------------------------------------------------------------------------------------------------------------------------------------
// Globals

// TODO
// getMoves bug: illegal moves in king vs king
// getMoves bug: illegal moves in king vs pawn
//
// ai tns
// add clock
//
// add move list navigation / suggestions
//
// options (clock, fps, difficulty, algo)
// credits
//
// cleanup / refactor / unit tests?
//
"use strict";

var glState, boardSquares, highlightedSquares;
var lastMove;
var gameMoveList = [];
var turn = 'white';
var stateList = {};
var setGameText, setGameSubText;
var cpuPlayer = null;

function WOPR() {
  var that = {};

  var params = {
    bloomAmount: .4,
    sunLight: 3.0,
    enabled: true,
    avgLuminance: 0.7,
    middleGrey: 0.04,
    maxLuminance: 16,
    adaptionRate: 0.2
  };

  var container;

  var camera, hudCamera, menuCamera, cameraCube;
  var renderer, dynamicHdrEffectComposer;
  var scene, menuScene, hudScene, sceneCube;

  var adaptToneMappingPass, hudScenePass, menuScenePass;
  var adaptiveLuminanceMat, currentLuminanceMat;

  var orbitControls, mouse;

  var audioManager;

  var earthMat = null;
  var texcanvas, earthTexCanvas;

  var playButton, optionsButton, creditsButton;

  var undoButton;

  var textureTable = {};
  var geometryTable = {};

  var board = [];
  boardSquares = [];
  glState = [];
  var selectedPieceX = null;
  var selectedPieceY = null;
  glState.whiteKingX = 4;
  glState.whiteKingY = 0;
  glState.blackKingX = 4;
  glState.blackKingY = 7;
  glState.capturedBlack = [];
  glState.capturedWhite = [];

  highlightedSquares = [];
  lastMove = [];

  for (var x = 0; x < 8; x++) {
    glState.push([null, null, null, null, null, null, null, null]);
  }

  for (var x = 0; x < 8; x++) {
    boardSquares.push([null, null, null, null, null, null, null, null]);
  }

  var stats = undefined;
  if (true && typeof window !== 'undefined') {
    stats = new Stats();
    document.body.appendChild(stats.domElement);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    stats.domElement.style.left = '0px';
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Load
  function loadAssets() {
    // Loading Manager
    THREE.DefaultLoadingManager.onStart = function (url, itemsLoaded, iTotal) {
      console.log('Started loading file: ' + url + '.\nLoaded ' + itemsLoaded +
          ' of ' + iTotal + ' files.');
      document.getElementById('debugConsole').innerHTML =
          'Loading Assets:<br> 0%';
    };

    THREE.DefaultLoadingManager.onLoad = function ( ) {
      console.log( 'Loading Complete!');
      document.getElementById('debugConsole').style = 'display: none;';
      init();
      animate();
    };

    THREE.DefaultLoadingManager.onProgress = function (url, itemsLoaded, iTotal) {
      console.log('Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' +
          iTotal + ' files.');
      document.getElementById('debugConsole').innerHTML = 'Loading Assets:<br> ' +
          Math.floor(100 * itemsLoaded / iTotal) + '%';
    };

    THREE.DefaultLoadingManager.onError = function (url) {
      console.log('There was an error loading ' + url);
      document.getElementById('debugConsole').innerHTML = 
        'Unknown Error:<br/> Unable to Load Game';
    };

    var textureLoader = new THREE.TextureLoader();
    textureTable['titleTexture'] = textureLoader.load(
        'textures/title.png');
    textureTable['menu1'] = textureLoader.load(
        'textures/menu.png');
    textureTable['menu2'] = textureLoader.load(
        'textures/menu.png');
    textureTable['menu3'] = textureLoader.load(
        'textures/menu.png');
    textureTable['hud1'] = textureLoader.load(
        'textures/hud1.png');
    textureTable['hud2'] = textureLoader.load(
        'textures/hud2.png');
    textureTable['hud3'] = textureLoader.load(
        'textures/hud3.png');
    textureTable['black_pawn'] = textureLoader.load(
        'textures/b_pawn_png_noShadow.png');
    textureTable['white_pawn'] = textureLoader.load(
        'textures/w_pawn_png_noShadow.png');
    textureTable['black_knight'] = textureLoader.load(
        'textures/b_knight_png_noShadow.png');
    textureTable['white_knight'] = textureLoader.load(
        'textures/w_knight_png_noShadow.png');
    textureTable['black_rook'] = textureLoader.load(
        'textures/b_rook_png_noShadow.png');
    textureTable['white_rook'] = textureLoader.load(
        'textures/w_rook_png_noShadow.png');
    textureTable['black_bishop'] = textureLoader.load(
        'textures/b_bishop_png_noShadow.png');
    textureTable['white_bishop'] = textureLoader.load(
        'textures/w_bishop_png_noShadow.png');
    textureTable['black_queen'] = textureLoader.load(
        'textures/b_queen_png_noShadow.png');
    textureTable['white_queen'] = textureLoader.load(
        'textures/w_queen_png_noShadow.png');
    textureTable['black_king'] = textureLoader.load(
        'textures/b_king_png_noShadow.png');
    textureTable['white_king'] = textureLoader.load(
        'textures/w_king_png_noShadow.png');

    var r = "textures/cube/MilkyWay/";
    var urls = [ r + "dark-s_px.jpg", r + "dark-s_nx.jpg",
           r + "dark-s_py.jpg", r + "dark-s_ny.jpg",
           r + "dark-s_pz.jpg", r + "dark-s_nz.jpg" ];

    textureTable['skyCube'] = new THREE.CubeTextureLoader().load(urls);
    textureTable['skyCube'].format = THREE.RGBFormat;

    // load audio
    audioManager = new AudioManager ({
        "menu": 'audio/tokyo-mask-death-drive.ogg',
      },
      function () {
        // audioManager.playSound("menu", true);
      });
    audioManager.load();
  }
  that.loadAssets = loadAssets;

  if (typeof window !== 'undefined') {
    var arx = window.innerWidth / 1920;
    var ary = window.innerHeight / 1080;
    var ar  = (arx / ary < 4 / 3) ? ary : arx;
    var osx = 2020 * ar;
    var osy = 1180 * ar;
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Init
  function init(withoutGraphics) {
    if (!withoutGraphics) {
      createScenes();
      createSkyBox();
      createMenu();
      createHUD();
    }

    for (var x = 0; x < 8; x++) {
      addPawn(glState, 'black', x, 6, withoutGraphics);
    }
    addRook(glState, 'black', 0, 7, withoutGraphics);
    addRook(glState, 'black', 7, 7, withoutGraphics);
    addKnight(glState, 'black', 1, 7, withoutGraphics);
    addKnight(glState, 'black', 6, 7, withoutGraphics);
    addBishop(glState, 'black', 2, 7, withoutGraphics);
    addBishop(glState, 'black', 5, 7, withoutGraphics);
    addQueen(glState, 'black', 3, 7, withoutGraphics);
    addKing(glState, 'black', 4, 7, withoutGraphics);

    for (var x = 0; x < 8; x++) {
      addPawn(glState, 'white', x, 1, withoutGraphics);
    }
    addRook(glState, 'white', 0, 0, withoutGraphics);
    addRook(glState, 'white', 7, 0, withoutGraphics);
    addKnight(glState, 'white', 1, 0, withoutGraphics);
    addKnight(glState, 'white', 6, 0, withoutGraphics);
    addBishop(glState, 'white', 2, 0, withoutGraphics);
    addBishop(glState, 'white', 5, 0, withoutGraphics);
    addQueen(glState, 'white', 3, 0, withoutGraphics);
    addKing(glState, 'white', 4, 0, withoutGraphics);

    if (!withoutGraphics) {
      createRenderer();
      attachControls();
      cpuPlayer = setupWorker();
    }

    gameMoveList.push([stateCopy(glState), 0, 0, 0, 0]);
  }
  that.init = init;

  function createScenes() {
    // CAMERAS
    camera = new THREE.PerspectiveCamera(70,
        window.innerWidth / window.innerHeight, 0.1, 100000);
    camera.position.x = 700;
    camera.position.y = 400;
    camera.position.z = 800;
    cameraCube = new THREE.PerspectiveCamera(70,
        window.innerWidth / window.innerHeight, 1, 100000);

    hudCamera = new THREE.OrthographicCamera(
        -window.innerWidth / 2, window.innerWidth / 2,
        window.innerHeight / 2, -window.innerHeight / 2,
        -10000, 10000);
    hudCamera.position.z = 100;

    menuCamera = new THREE.OrthographicCamera(
      -window.innerWidth / 2, window.innerWidth / 2,
      window.innerHeight / 2, -window.innerHeight / 2,
      -10000, 10000);
    menuCamera.position.z = 100;

    // SCENE
    scene = new THREE.Scene();
    menuScene = new THREE.Scene();
    hudScene = new THREE.Scene();
    sceneCube = new THREE.Scene();

    // LIGHTS
    var ambient = new THREE.AmbientLight(0x050505);
    scene.add(ambient);

    var directionalLight = new THREE.DirectionalLight(0xffffff, params.sunLight);
    directionalLight.position.set(2, 0, 10).normalize();
    directionalLight.intensity = params.sunLight;
    scene.add(directionalLight);
  }

  function createRenderer() {
    if (renderer) return;

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setFaceCulling(THREE.CullFaceNone);
    renderer.autoClear = false;

    renderer.gammaInput = true;
    renderer.gammaOutput = false;

    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    var height = window.innerHeight || 1;

    var parameters = { 
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      stencilBuffer: false
    };

    if (renderer.extensions.get('OES_texture_half_float_linear')) {
      parameters.type = THREE.FloatType;
    }
    var hdrRenderTarget = new THREE.WebGLRenderTarget(window.innerWidth, height,
        parameters);
    dynamicHdrEffectComposer = new THREE.EffectComposer(renderer,
        hdrRenderTarget);
    dynamicHdrEffectComposer.setSize(window.innerWidth, window.innerHeight);

    var scenePass = new THREE.RenderPass(scene, camera, undefined, undefined,
        undefined);
    hudScenePass = new THREE.RenderPass(hudScene, hudCamera);        
    menuScenePass = new THREE.RenderPass(menuScene, menuCamera);
    var skyboxPass = new THREE.RenderPass(sceneCube, cameraCube);
    scenePass.clear = false;
    hudScenePass.clear = false;
    hudScenePass.enabled = false;
    menuScenePass.clear = false;

    adaptToneMappingPass = new THREE.AdaptiveToneMappingPass(true, 256);
    adaptToneMappingPass.needsSwap = true;
    var bloomPass = new THREE.BloomPass();
    var gammaCorrectionPass = new THREE.ShaderPass(THREE.GammaCorrectionShader);
    gammaCorrectionPass.renderToScreen = true;

    dynamicHdrEffectComposer.addPass(skyboxPass);
    dynamicHdrEffectComposer.addPass(scenePass);
    dynamicHdrEffectComposer.addPass(hudScenePass);
    dynamicHdrEffectComposer.addPass(menuScenePass);
    dynamicHdrEffectComposer.addPass(adaptToneMappingPass);
    dynamicHdrEffectComposer.addPass(bloomPass);
    dynamicHdrEffectComposer.addPass(gammaCorrectionPass);

    bloomPass.copyUniforms["opacity"].value = params.bloomAmount;
    adaptToneMappingPass.setAdaptionRate(params.adaptionRate);
    adaptToneMappingPass.enabled = params.enabled;
    adaptToneMappingPass.setMaxLuminance(params.maxLuminance);
    adaptToneMappingPass.setMiddleGrey(params.middleGrey);
  }

  function attachControls() {
    // if (orbitControls) return;

    // orbitControls = new THREE.OrbitControls(camera, container);
    window.addEventListener('resize', onWindowResize, false);
    window.addEventListener('mousemove', onDocumentMouseMove, false);
    window.addEventListener('mouseup', onDocumentMouseClick, false);
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('keyup', onKeyUp);
  }

  function createSkyBox() {
    // Skybox
    adaptiveLuminanceMat = new THREE.ShaderMaterial( {
      uniforms: {
        map: { value: null }
      },
      vertexShader: document.getElementById('vBGShader').textContent,
      fragmentShader: document.getElementById('pBGShader').textContent,
      depthTest: false,
      blending: THREE.NoBlending
    } );

    currentLuminanceMat = new THREE.ShaderMaterial( {
      uniforms: {
        map: { value: null }
      },
      vertexShader: document.getElementById('vBGShader').textContent,
      fragmentShader: document.getElementById('pBGShader').textContent,
      depthTest: false
    } );

    var skyboxShader = THREE.ShaderLib["cube"];
    skyboxShader.uniforms["tCube"].value = textureTable['skyCube'];

    var skyboxMaterial = new THREE.ShaderMaterial( {
      fragmentShader: skyboxShader.fragmentShader,
      vertexShader: skyboxShader.vertexShader,
      uniforms: skyboxShader.uniforms,
      depthWrite: false,
      side: THREE.BackSide
    } ),

    mesh = new THREE.Mesh(new THREE.BoxGeometry( 100, 100, 100 ),
        skyboxMaterial);
    sceneCube.add(mesh);
  }

  function createMenu() {
    // menu items
    var geometry = new THREE.PlaneGeometry( 0.53 * osx, 0.94 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: textureTable['titleTexture'],
        transparent: true
      });
    var title = new THREE.Mesh(geometry, material);
    title.position.set(0, 0.092 * osy, 0);
    menuScene.add(title);

    var geometry = new THREE.PlaneGeometry(0.166 * osx, 0.051 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: textureTable['menu1'],
        transparent: true
      });
    material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
    material.map.repeat.x = 1 / 3;
    material.map.repeat.y = 1 / 3;
    material.map.offset.y = 2 / 3;
    material.map.offset.x = 2 / 3;
    playButton = new THREE.Mesh(geometry, material);
    playButton.position.set(0, -0.031 * osy, 0);
    menuScene.add(playButton);
    
    var geometry = new THREE.PlaneGeometry(0.166 * osx, 0.051 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: textureTable['menu2'],
        transparent: true
      });
    material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
    material.map.repeat.x = 1 / 3;
    material.map.repeat.y = 1 / 3;
    material.map.offset.y = 1 / 3;
    material.map.offset.x = 2 / 3;
    optionsButton = new THREE.Mesh(geometry, material);
    optionsButton.position.set(0, -0.102 * osy, 0);
    menuScene.add(optionsButton);

    var geometry = new THREE.PlaneGeometry(0.166 * osx, 0.051 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: textureTable['menu3'],
        transparent: true
      });
    material.map.wrapS = material.map.wrapT = THREE.RepeatWrapping;
    material.map.repeat.x = 1 / 3;
    material.map.repeat.y = 1 / 3;
    material.map.offset.y = 0 / 3;
    material.map.offset.x = 2 / 3;
    creditsButton = new THREE.Mesh(geometry, material);
    creditsButton.position.set(0, -0.172 * osy, 0);
    menuScene.add(creditsButton);
  }

  function createHUD() {
    // hud items
    // var geometry = new THREE.PlaneGeometry( 300, 300);
    // var material = new THREE.MeshBasicMaterial({
    //     map: textureTable['hud1'],
    //     transparent: true
    //   });
    // var hud1 = new THREE.Mesh(geometry, material);
    // hud1.position.set(-600, 100, 0);
    // hudScene.add(hud1);

    // var geometry = new THREE.PlaneGeometry( 400, 220);
    // var material = new THREE.MeshBasicMaterial({
    //     map: textureTable['hud2'],
    //     transparent: true
    //   });
    // var hud2 = new THREE.Mesh(geometry, material);
    // hud2.position.set(-600, -300, 0);
    // hudScene.add(hud2);

    // var geometry = new THREE.PlaneGeometry( 400, 600);
    // var material = new THREE.MeshBasicMaterial({
    //     map: textureTable['hud3'],
    //     transparent: true
    //   });
    // var hud3 = new THREE.Mesh(geometry, material);
    // hud3.position.set(650, 100, 0);
    // hudScene.add(hud3);

    var texCanvas = document.createElement('canvas');
    texCanvas.width = 512;
    texCanvas.height = 128;
    var ctx = texCanvas.getContext('2d');
    ctx.fillStyle = 'white';
    ctx.font = 'bold 24px Monospace';
    ctx.textAlign = 'center';
    ctx.fillText('White player\'s turn to play.', 256, 100);
    var texCanvasTexture = new THREE.CanvasTexture(texCanvas);

    var geometry = new THREE.PlaneGeometry(0.26666 * osx, 0.11851 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: texCanvasTexture,
        transparent: true
      });
    var hud3 = new THREE.Mesh(geometry, material);
    hud3.position.set(0, 0.3518 * osy, 0);
    hudScene.add(hud3);

    setGameText = function (text) {
      ctx.clearRect(0, 0, 512, 128);
      ctx.fillText(text, 256, 100);
      hud3.material.map.needsUpdate = true;
    }

    var subTexCanvas = document.createElement('canvas');
    subTexCanvas.width = 512;
    subTexCanvas.height = 128;
    var subCtx = subTexCanvas.getContext('2d');
    subCtx.fillStyle = 'lightgray';
    ctx.textAlign = 'center';
    subCtx.font = 'bold 20px Monospace';
    var subTexCanvasTexture = new THREE.CanvasTexture(subTexCanvas);

    var geometry = new THREE.PlaneGeometry(0.26666 * osx, 0.11851 * osy);
    var material = new THREE.MeshBasicMaterial({
        map: subTexCanvasTexture,
        transparent: true
      });
    var hud4 = new THREE.Mesh(geometry, material);
    hud4.position.set(0, 0.3287 * osy, 0);
    hudScene.add(hud4);

    setGameSubText = function (text) {
      var ctx = subTexCanvas.getContext('2d');
      ctx.clearRect(0, 0, 512, 128);
      ctx.textAlign = 'center';
      ctx.fillText(text, 256, 100);
      hud4.material.map.needsUpdate = true;
    }

    function clmult(cl1, cl2) {
      var r1 = cl1 / 0xffff;
      var g1 = (cl1 / 0xff) % 0xff;
      var b1 = cl1 % 0xff;
      var r2 = cl2 / 0xffff;
      var g2 = (cl2 / 0xff) % 0xff;
      var b2 = cl2 % 0xff;
      return (r1+r2)*0xffff + (g1+g2)*0xff + b1+b2;
    }

    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        var geometry = new THREE.PlaneGeometry(0.0416 * osx, 0.07407 * osy);
        var thisColor = ((x + y) % 2) ? 0xffffff : 0x555555;
        var material = new THREE.MeshBasicMaterial({
            color: thisColor,
            transparent: true
          });
        var square = new THREE.Mesh(geometry, material);
        square.position.set((x - 4) * 0.0416 * osx + 0.02083 * osx, (y - 4) * 0.07407 * osy, 0);
        square.knight_x = x;
        square.knight_y = y;

        function CreateSetHighlight(thisColor) {
          return function (value) {
            if (value) {
              this.material.color.set( clmult(0x77aa33, thisColor) );
            }
            else {
              this.material.color.set(thisColor);
            }
          }
        }
        square.setHighlight = CreateSetHighlight(thisColor);

        board.push(square);
        boardSquares[x][y] = square;
        hudScene.add(square);
      }
    }

    return that;
  }

  function pieceCount(state, color) {
    var cnt = 0;
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y] && state[x][y].color == color) {
          cnt += 1;
        }
      }
    }
    return cnt;
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // AI

  function alanEvaluation(state) {
    var res = 0;
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y] && state[x][y].color == 'black') {
          res -= state[x][y].value;
        }
        else if (state[x][y]) {
          res += state[x][y].value;
        }
      }
    }
    return res;
  }

  function getSuggestedMoves(state, color, depth) {
    var stateInCheck = isInCheck(state, color);
    var moveList = [];
    var totalPieces = pieceCount(state, color);
    var curPiece = 0;
    // for each piece of correct color
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y] && state[x][y].color == color) {
          curPiece += 1;
          if (depth == 3) {
            console.log(curPiece/totalPieces);
          }
          // for each move available
          var mlist = state[x][y].getMoves(state, x, y);
          for (var i in mlist) {
            var tempState = stateCopy(state);
            tempState[x][y].goTo(tempState, x, y, mlist[i][0], mlist[i][1]);
            if (stateInCheck && isInCheck(tempState, color)) {
              // if move is invalid ignore it
              continue;
            }
            if (depth > 0) {
              // digg deeper
              var responses = getSuggestedMoves(tempState, (color == 'black') ? 'white' : 'black', depth - 1);
              if (responses.length) {
                moveList.push([responses[0][0], [x, y], mlist[i]]);
              }
            }
            else {
              // evaluate at this point             
              moveList.push([alanEvaluation(tempState), [x, y], mlist[i]]);
            }
          }
        }
      }
    }
    moveList.sort(compareFunction);
    if (color == 'white') {
      moveList.reverse();
    }
    return moveList.slice(0, 10);
  }
  that.getSuggestedMoves = getSuggestedMoves;

  function getSuggestedMove(state, color, depth) {
    return getSuggestedMoves(state, color, depth)[0];
  }


  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Main functionality

  function isThreatened(state, color, x, y, xx, xy) {
    var kx = x;
    var ky = y;
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if ((x != xx || y != xy) &&
            state[x][y] && state[x][y].color != color && !state[x][y].isKing && state[x][y].canMove(state, x, y, kx, ky)) {
          return true;
        }
      }
    }
    return false;
  }

  function isInCheck(state, color) {
    var kx, ky;
    if (color == 'black') {
      kx = state.blackKingX;
      ky = state.blackKingY;
    }
    else {
      kx = state.whiteKingX;
      ky = state.whiteKingY;
    }

    return isThreatened(state, color, kx, ky, kx, ky);
  }

  function stateCopy(state) {
    var copy = [];
    for (var x = 0; x < 8; x++) {
      copy.push([null, null, null, null, null, null, null, null]);
    }
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y]) {
          copy[x][y] = Object.assign({}, state[x][y]);
        }
        else {
          copy[x][y] = null;
        }
      }
    }
    copy.blackKingX = state.blackKingX;
    copy.blackKingY = state.blackKingY;
    copy.whiteKingX = state.whiteKingX
    copy.whiteKingY = state.whiteKingY;
    copy.capturedBlack = listCopy(state.capturedBlack);
    copy.capturedWhite = listCopy(state.capturedWhite);
    return copy;
  }

  that.stateCopy = stateCopy;

  function listCopy(list) {
    var copy = [];
    for (var x in list) {
      copy.push(list[x]);
    }
    return copy;
  }

  function isInMate(state, color) {
    var kx, ky;
    if (color == 'black') {
      kx = state.blackKingX;
      ky = state.blackKingY;
    }
    else {
      kx = state.whiteKingX;
      ky = state.whiteKingY;
    }
    
    if (!isInCheck(state, color)) {
      return false;
    }

    // iterate board
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        // if on our team
        if (state[x][y] && state[x][y].color == color) {
          // try every move
          var moveList = state[x][y].getMoves(state, x, y);
          for (let move of moveList) {
            // store state
            var tempState = stateCopy(state);
            // apply move
            tempState[x][y].goTo(tempState, x, y, move[0], move[1]);
            if (!isInCheck(tempState, color)) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }

  function hasNoMoves(state, color) {
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y] && state[x][y].color == color) {
          if (state[x][y].getMoves(state, x, y).length) {
            return false;
          }
        }
      }
    }
    return true;
  }

  function canMove(state, sx, sy, dx, dy) {
    var moveList = this.getMoves(state, sx, sy);
    for (let move of moveList) {
      if (move[0] == dx && move[1] == dy) {
        return true;
      }
    }
    return false;
  }

  function compareFunction(a, b) {
    if (a[0] < b[0]) {
      return -1;
    }
    else if (b[0] < a[0]) {
      return 1;
    }
    else {
      if (a[1] < b[1]) {
        return -1;
      }
      else if (b[1] < a[1]) {
        return 1;
      }
      else {
        if (a[2] < b[2]) {
          return -1;
        }
        else if (b[2] < a[2]) {
          return 1;
        }
        else {
          return 0;
        }
      }
    }
  }

  function hashState(state) {
    var wlist = [];
    var blist = [];
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y]) {
          if (state[x][y].color == 'black') {
            blist.push([state[x][y].value, x, y]);
          }
          else {
            wlist.push([state[x][y].value, x, y]);
          }
        }
      }
    }
    wlist.sort(compareFunction);
    blist.sort(compareFunction);
    return "".concat(wlist.concat(blist));
  }

  function hasDrawnDueToRepeatedPosition(moveList) {
    stateList = {};
    for (let move of moveList) {
      var h = hashState(move[0]);
      stateList[h] = (stateList[h] ? stateList[h] : 0) + 1;
    }
    for (var key in stateList) {
      if (stateList[key] >= 3) {
        return true;
      }
    }
    return false;
  }

  function removeFromBoard(state, x, y) {
    // TODO: destroy?
    if (!state[x][y]) {
      return;
    }
    if (state[x][y].color == 'black') {
      state.capturedBlack.push(state[x][y]);
    }
    else {
      state.capturedWhite.push(state[x][y]);
    }
  }

  function hasCapturedMesh(state, mesh) {
    for (let obj of state.capturedBlack) {
      if (obj.mesh == mesh) {
        return true;
      }
    }
    for (let obj of state.capturedWhite) {
      if (obj.mesh == mesh) {
        return true;
      }
    }
    return false;
  }

  function cleanUpUnusedPieces(state) {
    var foundList = [];
    for (var x = 0; x < 8; x++) {
      for (var y = 0; y < 8; y++) {
        if (state[x][y]) {
          foundList.push(state[x][y].mesh);
        }
      }
    }
    var toRemove = [];
    for (let child of hudScene.children) {
      var notFound = foundList.indexOf(child) < 0;
      var notCaptured = !hasCapturedMesh(state, child);
      var isPiece = child.piece;
      if (notFound && notCaptured && isPiece) {
        toRemove.push(child)
      }
    }
    for (let child of toRemove) {
      hudScene.remove(child);
    }
  }
  that.cleanUpUnusedPieces = cleanUpUnusedPieces;

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Worker code

  function onMessageReceived(e) {
    // sync states
    var sx = e.data[0];
    var sy = e.data[1];
    var dx = e.data[2];
    var dy = e.data[3];
    glState[sx][sy].goTo(glState, sx, sy, dx, dy);
    // compute response & answer
    var m = getSuggestedMove(glState, 'black', 2);
    if (m) {
      var src = m[1];
      var dst = m[2];
      glState[src[0]][src[1]].goTo(glState, src[0], src[1], dst[0], dst[1]);
      postMessage(m);
    }
  }
  that.onMessageReceived = onMessageReceived;

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Main Thread code

  function setupWorker() {
    var worker = new Worker("js/wargames.js");

    worker.onmessage = function(e) {
      console.log(e);
      // get response from worker
      var m = e.data;
      // play response
      if (m) {
        var src = m[1];
        var dst = m[2];
        glState[src[0]][src[1]].goTo(glState, src[0], src[1], dst[0], dst[1], true);
        turn = (turn == 'white') ? 'black' : 'white';

        // TODO: find a better solution to this
        cleanUpUnusedPieces(glState);

        if (turn == 'white') {
          setGameText('White player\'s turn to play.');
        }
        else {
          setGameText('Black player\'s turn to play.');
        }

        if (isInCheck(glState, turn)) {
          console.log('Woo hoo. They are in check.');
          setGameSubText(turn + " is in check");
        }
        if (isInMate(glState, turn)) {
          console.log('Woo hoo. They are fucked.');
          setGameSubText(turn + " is in mate");
        }
        if (hasNoMoves(glState, turn)) {
          console.log('Draw. They can not move.');
          setGameSubText("It's a draw");
        }
        if (hasDrawnDueToRepeatedPosition(gameMoveList)) {
          console.log('Draw. Repeated State.');
          setGameSubText("It's a draw. Repeated game state 3 times.");
        }
        lastMove = [[src[0], src[1]], [dst[0], dst[1]]];
      }
    }

    return worker;
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Pieces

  function addPawn(state, color, posX, posY, withoutGraphics) {
    var pawn = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_pawn'] : textureTable['white_pawn'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      mesh.piece = true;
      pawn.mesh = mesh;
      hudScene.add(pawn.mesh);
    }
    pawn.enPassant = null;

    function getMovesWhite(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x; cy = y + 1;
      if (cy <  8 && !state[cx][cy]) {
        res.push([cx, cy]);
        if (y == 1) {
          cy = y + 2;        
          if (!state[cx][cy]) {
            res.push([cx, cy]);
          }
        }
      }
      cx = x - 1; cy = y + 1;
      if (cx > -1 && cy <  8 && state[cx][cy] && state[cx][cy].color != color) {
        res.push([cx, cy]);
      }
      cx = x + 1; cy = y + 1;
      if (cx <  8 && cy <  8 && state[cx][cy] && state[cx][cy].color != color) {
        res.push([cx, cy]);
      }
      if (pawn.enPassant) {
        res.push(pawn.enPassant);
      }
      return res;
    }

    function getMovesBlack(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x; cy = y - 1;
      if (cy > -1 && !state[cx][cy]) {
        res.push([cx, cy]);
        if (y == 6) {
          cy = y - 2;        
          if (!state[cx][cy]) {
            res.push([cx, cy]);
          }
        }
      }
      cx = x - 1; cy = y - 1;
      if (cx > -1 && cy > -1 && state[cx][cy] && state[cx][cy].color != color) {
        res.push([cx, cy]);
      }
      cx = x + 1; cy = y - 1;
      if (cx <  8 && cy > -1 && state[cx][cy] && state[cx][cy].color != color) {
        res.push([cx, cy]);
      }
      if (pawn.enPassant) {
        res.push(pawn.enPassant);
      }
      return res;
    }

    function goToWhite(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[sx][sy].enPassant = null;
      if (dy != 7) {
        state[dx][dy] = state[sx][sy];
        state[sx][sy] = null;
        if (sy == 1 && dy == 3) {
          if (dx + 1 <  8 && state[dx + 1][dy] && state[dx + 1][dy].isPawn) {
            state[dx + 1][dy].enPassant = [dx, dy - 1];
          }
          if (dx - 1 > -1 && state[dx - 1][dy] && state[dx - 1][dy].isPawn) {
            state[dx - 1][dy].enPassant = [dx, dy - 1];
          }
        }
        if (pawn.enPassant && pawn.enPassant[0] == dx && pawn.enPassant[1] == dy) {
          removeFromBoard(state, dx, dy - 1);
          state[dx][dy - 1] = null;
        }
      }
      else {
        // TODO: pick
        state[sx][sy] = null;        
        addQueen(state, 'white', dx, dy, withoutGraphics);
        // hudScene.remove(state[sx][sy].mesh);
      }
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    function goToBlack(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[sx][sy].enPassant = null;
      if (dy != 0) {
        state[dx][dy] = state[sx][sy];
        state[sx][sy] = null;
        if (sy == 6 && dy == 4) {
          if (dx + 1 <  8 && state[dx + 1][dy] && state[dx + 1][dy].isPawn) {
            state[dx + 1][dy].enPassant = [dx, dy + 1];
          }
          if (dx - 1 > -1 && state[dx - 1][dy] && state[dx - 1][dy].isPawn) {
            state[dx - 1][dy].enPassant = [dx, dy + 1];
          }
        }
        if (pawn.enPassant && pawn.enPassant[0] == dx && pawn.enPassant[1] == dy) {
          removeFromBoard(state, dx, dy + 1);
          state[dx][dy + 1] = null;
        }
      }
      else {
        // TODO: pick
        // TODO: add state in add**** functions as arg
        state[sx][sy] = null;        
        addQueen(state, 'black', dx, dy, withoutGraphics);
        // hudScene.remove(state[sx][sy].mesh);
      }
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    pawn.getMoves = (color == 'black') ? getMovesBlack : getMovesWhite;
    pawn.canMove = canMove;
    pawn.goTo = (color == 'black') ? goToBlack : goToWhite;

    pawn.color = color;
    pawn.isPawn = true;
    pawn.value = 1;

    state[posX][posY] = pawn;
  }

  function addKnight(state, color, posX, posY, withoutGraphics) {
    var knight = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_knight'] : textureTable['white_knight'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      mesh.piece = true;
      knight.mesh = mesh;
      hudScene.add(knight.mesh);
    }

    function getMoves(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x + 1; cy = y + 2;
      if (cx <  8 && cy <  8 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x - 1; cy = y + 2;
      if (cx > -1 && cy <  8 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x + 2; cy = y + 1;
      if (cx <  8 && cy <  8 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x - 2; cy = y + 1;
      if (cx > -1 && cy <  8 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x + 1; cy = y - 2;
      if (cx <  8 && cy > -1 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x - 1; cy = y - 2;
      if (cx > -1 && cy > -1 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x + 2; cy = y - 1;
      if (cx <  8 && cy > -1 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      cx = x - 2; cy = y - 1;
      if (cx > -1 && cy > -1 && (!state[cx][cy] || state[cx][cy].color != color)) {
        res.push([cx, cy]);
      }
      return res;
    }

    function goTo(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    knight.getMoves = getMoves;
    knight.canMove = canMove;
    knight.goTo = goTo;

    knight.color = color;
    knight.value = 3;

    state[posX][posY] = knight;
  }

  function addRook(state, color, posX, posY, withoutGraphics) {
    var rook = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_rook'] : textureTable['white_rook'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      mesh.piece = true;
      rook.mesh = mesh;
      hudScene.add(rook.mesh);
    }

    function getMoves(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x + 1;
      cy = y;
      while (cx < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
      }
      cx = x - 1;
      while (cx > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx -= 1;
      }
      cx = x;
      cy = y + 1;      
      while (cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy += 1;
      }
      cy = y - 1;
      while (cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy -= 1;
      }

      return res;
    }

    function goTo(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      state[dx][dy].hasNotMoved = false;
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    rook.getMoves = getMoves;
    rook.canMove = canMove;
    rook.goTo = goTo;

    rook.color = color;
    rook.hasNotMoved = true;
    rook.value = 5;

    state[posX][posY] = rook;
  }

  function addBishop(state, color, posX, posY, withoutGraphics) {
    var bishop = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_bishop'] : textureTable['white_bishop'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      mesh.piece = true;
      bishop.mesh = mesh;
      hudScene.add(bishop.mesh);
    }

    function getMoves(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x + 1;
      cy = y + 1;
      while (cx < 8 && cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
        cy += 1;
      }
      cx = x - 1;
      cy = y + 1;
      while (cx > -1 && cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx -= 1;
        cy += 1;
      }
      cx = x + 1;
      cy = y - 1;      
      while (cx < 8 && cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
        cy -= 1;
      }
      cx = x - 1
      cy = y - 1;
      while (cx > -1 && cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy -= 1;
        cx -= 1;
      }

      return res;
    }

    function goTo(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    bishop.getMoves = getMoves;
    bishop.canMove = canMove;
    bishop.goTo = goTo;

    bishop.color = color;
    bishop.value = 3.1;

    state[posX][posY] = bishop;
  }

  function addQueen(state, color, posX, posY, withoutGraphics) {
    var queen = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_queen'] : textureTable['white_queen'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      mesh.piece = true;
      queen.mesh = mesh;
      hudScene.add(queen.mesh);
    }

    function getMoves(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x + 1;
      cy = y;
      while (cx < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
      }
      cx = x - 1;
      while (cx > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx -= 1;
      }
      cx = x;
      cy = y + 1;      
      while (cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy += 1;
      }
      cy = y - 1;
      while (cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy -= 1;
      }
      // ---
      cx = x + 1;
      cy = y + 1;
      while (cx < 8 && cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
        cy += 1;
      }
      cx = x - 1;
      cy = y + 1;
      while (cx > -1 && cy < 8) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx -= 1;
        cy += 1;
      }
      cx = x + 1;
      cy = y - 1;      
      while (cx < 8 && cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cx += 1;
        cy -= 1;
      }
      cx = x - 1
      cy = y - 1;
      while (cx > -1 && cy > -1) {
        if (!state[cx][cy]) {
          res.push([cx, cy]);
        }
        else if (state[cx][cy].color != color) {
          res.push([cx, cy]);
          break;
        }
        else {
          break;
        }
        cy -= 1;
        cx -= 1;
      }

      return res;
    }

    function goTo(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    queen.getMoves = getMoves;
    queen.canMove = canMove;
    queen.goTo = goTo;

    queen.color = color;
    queen.value = 9;

    state[posX][posY] = queen;
  }

  function addKing(state, color, posX, posY, withoutGraphics) {
    var king = {};
    if (!withoutGraphics) {
      var geometry = new THREE.PlaneGeometry(0.03125 * osx, 0.05555 * osy);
      var material = new THREE.MeshBasicMaterial({
          map: color == 'black' ? textureTable['black_king'] : textureTable['white_king'],
          transparent: true
        });
      var mesh = new THREE.Mesh(geometry, material);
      king.mesh = mesh;
      mesh.piece = true;
      hudScene.add(king.mesh);
    }

    function getMoves(state, x, y) {
      var res = [];
      var cx, cy;
      cx = x + 1;
      cy = y;
      if ((cx < 8) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cx = x - 1;
      if ((cx > -1) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cx = x;
      cy = y + 1;      
      if ((cy < 8) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cy = y - 1;
      if ((cy > -1) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      // ---
      cx = x + 1;
      cy = y + 1;
      if ((cx <  8 && cy <  8) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cx = x - 1;
      cy = y + 1;
      if ((cx > -1 && cy <  8) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cx = x + 1;
      cy = y - 1;      
      if ((cx <  8 && cy > -1) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      cx = x - 1
      cy = y - 1;
      if ((cx > -1 && cy > -1) && (!state[cx][cy] || state[cx][cy].color != color) && !isThreatened(state, color, cx, cy, x, y)) {
          res.push([cx, cy]);
      }
      // castling
      if (color == 'white' && king.hasNotMoved && state[7][0] && state[7][0].hasNotMoved &&
          state[5][0] == null && state[6][0] == null && !isInCheck(state, color) &&
          !isThreatened(state, color, 5, 0, x, y) && !isThreatened(state, color, 6, 0, x, y)) {
          res.push([6, 0]);
      }
      if (color == 'white' && king.hasNotMoved && state[0][0] && state[0][0].hasNotMoved &&
          state[3][0] == null && state[2][0] == null && state[1][0] == null &&
          !isInCheck(state, color) &&
          !isThreatened(state, color, 3, 0, x, y) && !isThreatened(state, color, 2, 0, x, y)) {
          res.push([2, 0]);
      }
      if (color == 'black' && king.hasNotMoved && state[7][7] && state[7][7].hasNotMoved &&
          state[5][7] == null && state[6][7] == null && !isInCheck(state, color) &&
          !isThreatened(state, color, 5, 7, x, y) && !isThreatened(state, color, 6, 7, x, y)) {
          res.push([6, 7]);
      }
      if (color == 'black' && king.hasNotMoved && state[0][7] && state[0][7].hasNotMoved &&
          state[3][7] == null && state[2][7] == null && state[1][7] == null &&
          !isInCheck(state, color) &&
          !isThreatened(state, color, 3, 7, x, y) && !isThreatened(state, color, 2, 7, x, y)) {
          res.push([2, 7]);
      }

      return res;
    }

    function goToWhite(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      state.whiteKingX = dx;
      state.whiteKingY = dy;
      state[dx][dy].hasNotMoved = false;
      if (sx == 4 && sy == 0 && dx == 6 && dy == 0) {
        state[7][0].goTo(state, 7, 0, 5, 0);
      }
      else if (sx == 4 && sy == 0 && dx == 2 && dy == 0) {
        state[0][0].goTo(state, 0, 0, 3, 0);
      }
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    function goToBlack(state, sx, sy, dx, dy, registerMove) {
      removeFromBoard(state, dx, dy);
      state[dx][dy] = state[sx][sy];
      state[sx][sy] = null;
      state.blackKingX = dx;
      state.blackKingY = dy;
      state[dx][dy].hasNotMoved = false;
      if (sx == 4 && sy == 7 && dx == 6 && dy == 7) {
        state[7][7].goTo(state, 7, 7, 5, 7);
      }
      else if (sx == 4 && sy == 7 && dx == 2 && dy == 7) {
        state[0][7].goTo(state, 0, 7, 3, 7);
      }
      if (registerMove) {
        gameMoveList.push([stateCopy(state), sx, sy, dx, dy]);
      }
    }

    king.getMoves = getMoves;
    king.canMove = canMove;
    king.goTo = (color == 'black') ? goToBlack : goToWhite;

    king.color = color;
    king.hasNotMoved = true;
    king.isKing = true;
    king.value = 1000;

    state[posX][posY] = king;
  }


  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Util functions
  function onWindowResize() {
    hudCamera.aspect = window.innerWidth / window.innerHeight;
    hudCamera.updateProjectionMatrix();

    menuCamera.aspect = window.innerWidth / window.innerHeight;
    menuCamera.updateProjectionMatrix();    

    cameraCube.aspect = window.innerWidth / window.innerHeight;
    cameraCube.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // render
  var rt = 0;
  var drt = 0.0001;
  function animate(dt) {
    requestAnimationFrame(animate);
    adaptiveLuminanceMat.uniforms.map.value = adaptToneMappingPass.luminanceRT;
    currentLuminanceMat.uniforms.map.value = adaptToneMappingPass.currentLuminanceRT;

    // orbitControls.update();
    var state = glState;
    for (var x in state) {
      for (var y in state[x]) {
        if (state[x][y]) {
          state[x][y].mesh.position.set((x - 4) * 0.0416 * osx + 0.02083 * osx, (y - 4) * 0.07407 * osy, 0);
        }
      }
    }

    for (var y in glState.capturedBlack) {
      glState.capturedBlack[y].mesh.position.set((- 5 - Math.floor(y/8)) * 0.0416 * osx + 0.01083 * osx, (y % 8 - 4) * 0.07407 * osy, 0);
    }

    for (var y in glState.capturedWhite) {
      glState.capturedWhite[y].mesh.position.set((4.5 + Math.floor(y/8)) * 0.0416 * osx + 0.01083 * osx, (- y % 8 + 3) * 0.07407 * osy, 0);
    }

    for (let square of board) {
      square.setHighlight(false);
    }

    for (let pos of highlightedSquares) {
      boardSquares[pos[0]][pos[1]].setHighlight(true);
    }

    for (let pos of lastMove) {
      boardSquares[pos[0]][pos[1]].setHighlight(true);
    }

    if (stats) {
      stats.update();
    }

    render();
  }

  function render() {
    camera.lookAt(scene.position);
    cameraCube.rotation.copy(camera.rotation);

    dynamicHdrEffectComposer.render(0.017);
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Controls
  function onDocumentMouseMove(event) {
    event.preventDefault();
    event.stopPropagation();

    var height = window.innerHeight;
    var width = window.innerWidth;
    mouse = {};
    mouse.x = (event.clientX / width) * 2 - 1;
    mouse.y = - (event.clientY / height) * 2 + 1;
    var raycaster = new THREE.Raycaster();
    var m = new THREE.Vector3(mouse.x, mouse.y, 0.5);

    // Check menu
    if (menuScenePass.enabled) {
      raycaster.setFromCamera(m, menuCamera);
      var intersects = raycaster.intersectObjects([playButton, optionsButton,
          creditsButton]);
      playButton.material.map.offset.x = 0;
      optionsButton.material.map.offset.x = 0;
      creditsButton.material.map.offset.x = 0;

      for (let button of [playButton, optionsButton, creditsButton]) {
        button.material.map.offset.x = 2 / 3;
      }

      if (intersects.length) {
        intersects[0].object.material.map.offset.x = 1 / 3;
        return false;
      }
    }

    // Check ui elements

    // Check 3d World
    raycaster.setFromCamera(m, hudCamera);

    var intersects = raycaster.intersectObjects(board);
    for (let sq of board) {
      sq.material.opacity = 0.8;
    }
    if (intersects.length) {
      intersects[0].object.material.opacity = 1.0;
    } else {
      
    }

    return false;
  }

  var pA = null;
  function onDocumentMouseClick(event) {
    event.preventDefault();
    event.stopPropagation();

    var height = window.innerHeight;
    var width = window.innerWidth;
    mouse = {};
    mouse.x = (event.clientX / width) * 2 - 1;
    mouse.y = - (event.clientY / height) * 2 + 1;
    var raycaster = new THREE.Raycaster();
    var m = new THREE.Vector3(mouse.x, mouse.y, 0.5);

    // Check menu
    if (menuScenePass.enabled) {
      raycaster.setFromCamera(m, menuCamera);
      var intersects = raycaster.intersectObjects([playButton, optionsButton,
          creditsButton]);
      playButton.material.map.offset.x = 0;
      optionsButton.material.map.offset.x = 0;
      creditsButton.material.map.offset.x = 0;
      if (intersects.length) {
        menuScenePass.enabled = false;
        hudScenePass.enabled = true;
        return false;
      }
    }

    // Check ui elements

    var state = glState;
    // Check 3d World
    raycaster.setFromCamera(m, hudCamera);
    var intersects = raycaster.intersectObjects(board);
    if (intersects.length) {
      if ((selectedPieceX === null || selectedPieceY === null) &&
          state[intersects[0].object.knight_x][intersects[0].object.knight_y]) {

        selectedPieceX = intersects[0].object.knight_x;
        selectedPieceY = intersects[0].object.knight_y;
        highlightedSquares = state[intersects[0].object.knight_x][intersects[0].object.knight_y].getMoves(state, selectedPieceX, selectedPieceY);
      }
      else if (selectedPieceX !== null && selectedPieceY !== null) {
        if ((intersects[0].object.knight_x != selectedPieceX || intersects[0].object.knight_y != selectedPieceY) &&
             state[selectedPieceX][selectedPieceY].canMove(state, selectedPieceX, selectedPieceY, intersects[0].object.knight_x, intersects[0].object.knight_y) &&
             state[selectedPieceX][selectedPieceY].color == turn) {

          var tempState = stateCopy(state);
          var color = tempState[selectedPieceX][selectedPieceY].color;
          tempState[selectedPieceX][selectedPieceY].goTo(tempState, selectedPieceX, selectedPieceY, intersects[0].object.knight_x, intersects[0].object.knight_y);
          var otherColor = (color == 'white') ? 'black' : 'white';
          setGameSubText('');
          if (isInCheck(tempState, color)) {
            console.log('We are in check. We fucked up. revert last move.');
            return true;
          }
          if (isInCheck(tempState, otherColor)) {
            console.log('Woo hoo. They are in check.');
            setGameSubText(otherColor + " is in check");
          }
          if (isInMate(tempState, otherColor)) {
            console.log('Woo hoo. They are fucked.');
            setGameSubText(otherColor + " is in mate");
          }
          if (hasNoMoves(tempState, otherColor)) {
            console.log('Draw. They can not move.');
            setGameSubText("It's a draw");
          }
          state[selectedPieceX][selectedPieceY].goTo(state, selectedPieceX, selectedPieceY, intersects[0].object.knight_x, intersects[0].object.knight_y, true);
          if (hasDrawnDueToRepeatedPosition(gameMoveList)) {
            console.log('Draw. Repeated State.');
            setGameSubText("It's a draw. Repeated game state 3 times.");
          }
          lastMove = [[selectedPieceX, selectedPieceY], [intersects[0].object.knight_x, intersects[0].object.knight_y]];
          turn = (turn == 'white') ? 'black' : 'white';

          // cpu player/worker starts thinking
          cpuPlayer.postMessage([selectedPieceX, selectedPieceY, intersects[0].object.knight_x, intersects[0].object.knight_y]);

          // TODO: find a better solution to this
          cleanUpUnusedPieces(glState);

          if (turn == 'white') {
            setGameText('White player\'s turn to play.');
          }
          else {
            setGameText('Black player\'s turn to play.');
          }
        }
        selectedPieceY = null;
        selectedPieceX = null;
        highlightedSquares = [];
      }
    } else {
      selectedPieceY = null;
      selectedPieceX = null;
      highlightedSquares = [];
    }

    return false;
  }

  var actions = {};
  var keysActions = {
    "Escape":'menu'
  };

  function onKeyUp(e) {
    if(keysActions[e.code]) {
      actions[keysActions[e.code]] = false;
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  function onKeyDown(e) {
    if(keysActions[e.code]) {
      actions[keysActions[e.code]] = true;
      if(keysActions[e.code] == "menu") {
        menuScenePass.enabled = true;
        hudScenePass.enabled = false;
      }
      e.preventDefault();
      e.stopPropagation();
      return false;
    }
  }

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Load and go
  // loadAssets();
  return that;
}

var game = WOPR();
game.init(true);
onmessage = game.onMessageReceived;