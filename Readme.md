WHAT IS IT
----------

Simple chess game written in javascript using THREE.js (WebGL).
It implements a variant of Turing's solution.